package cn.itheima.service;

import cn.itheima.entity.PageQueryBean;

import java.util.Map;

public interface UserService {

    Map pageQuery(PageQueryBean pageQueryBean);

    void deleteUserById(Integer id);

    Map findById(Integer id);
}
