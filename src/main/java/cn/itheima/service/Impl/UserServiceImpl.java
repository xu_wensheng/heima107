package cn.itheima.service.Impl;

import cn.itheima.entity.PageQueryBean;
import cn.itheima.mapper.UserMapper;
import cn.itheima.pojo.User;
import cn.itheima.service.UserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Transactional
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    Map map = new HashMap<>();
    //   Mybatis插件分页
    @Override
    public Map pageQuery(PageQueryBean pageQueryBean) {
        //  分页参数 传递 PageHelper
        PageHelper.startPage(pageQueryBean.getPageNum(),pageQueryBean.getPageSize());
        List<User> userList=userMapper.findAll(pageQueryBean.getQueryString());
        PageInfo<User> pageInfo=new PageInfo<User>(userList);
        map.put("userlist",pageInfo.getList());
        map.put("total",pageInfo.getTotal());
        return map;
    }

    @Override
    public void deleteUserById(Integer id) {
        userMapper.deleteUserById(id);
    }

    @Override
    public Map findById(Integer id) {
        Boolean falg;
       List<User> users= userMapper.findById(id);
        if (users==null){
            falg=false;
        }else {
            falg=true;
        }
        map.put("falg" ,falg);
        return map;
    }


}
