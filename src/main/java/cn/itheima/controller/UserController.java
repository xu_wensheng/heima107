package cn.itheima.controller;

import cn.itheima.entity.PageQueryBean;
import cn.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;
    @RequestMapping("/pageQuery.do")
    public Map pageQuery(@RequestBody PageQueryBean pageQueryBean){
        Map map=userService.pageQuery(pageQueryBean);
        return map;
    }
    @RequestMapping("/deleteUserById.do")
    public Map deleteUserById(@RequestParam("id") Integer id ){
        userService.deleteUserById(id);
        Map map=userService.findById(id);
        return map;
    }
}
