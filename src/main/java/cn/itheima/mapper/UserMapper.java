package cn.itheima.mapper;


import cn.itheima.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    List<User> findAll(@Param("queryString") String queryString);
    List<User> pageQuery(@Param("startIndex") int startIndex, @Param("pageSize") Integer pageSize);

    long findTotalCounts();

    void deleteUserById(@Param("id") Integer id);

    List<User> findById(@Param("id") Integer id);
}
